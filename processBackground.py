# Extract the average image for the detectors present in the configuration file and save it in the background folder

from argparse import ArgumentParser
from getpass import getuser  # to get the username
import os
import sys


def createRunJobFile(makeTagStr, imgAvgStr, workingFolder, runID):
    """ Creates the file to be submitted for the JOB and submits it. """
    cmdFile = os.path.join(workingFolder, 'JOB%d' % runID)
    with open(cmdFile, 'w') as f:
        f.write('#!/bin/bash\n')
        f.write('#\n')
        f.write('#PBS -l walltime=1:00:00\n')
        f.write('#PBS -l ncpus=1\n')
        f.write('#PBS -q serial\n')
        f.write('cd $PBS_O_WORKDIR\n')  # go to the same working directory as the one in which the script was launched
        f.write(makeTagStr + '\n')
        f.write(imgAvgStr + '\n')
    os.system('chmod +x %s' % cmdFile)
    os.system('qsub %s' % cmdFile)


def getDetectorsForRun(runID, bl=3):
    """
    Get the detectors present in the data for the run and beamline
    Parameters
    ----------
    run: the run identifier
    bl: the beamline (default 3)

    Returns
    -------
    A list of detectors present
    """
    tmpFileName = 'out.out'
    res = os.system('ShowDetIDList -b %d -r %d > %s' % (bl, runID, tmpFileName))
    if not res == 0:  # failure
        print('WARNING: Cannot determine the detectors for run #%d (bl%d)' % (runID, bl))
        return []
    with open(tmpFileName) as f:
        mylist = f.read().splitlines()
    return mylist


def checkAndCreateFolder(folderName):
    """
    Checks and creates if necessary the folder needed.
    Returns success (bool)
    """
    if not os.path.isdir(folderName):
        try:
            os.mkdir(folderName)
        except Exception as e:
            print('ERROR: cannot create the run folder %s (%s)' % (folderName, e))
            return False
    return True


if __name__ == '__main__':
    """ Entrance method. """
    user = getuser()
    parser = ArgumentParser(description='Creates an average image file to be used as background.')

    # Run number
    parser.add_argument('-r', type=int, dest='run', required=True, help='The run number to process')
    # Beamline
    parser.add_argument('-b', type=int, dest='beamline', default=3, help='The beamline (default 3)')
    # Detector(s)
    parser.add_argument('-det', type=str, dest='detectors', default='', help='The detectors to be averaged. Default ALL')
    # Conditions
    parser.add_argument('-inp', type=str, dest='conditions', default='', help='Full path to the Conditions file to be used. Default None')
    # Data directory
    parser.add_argument('-dir', type=str, dest='mainDirID', choices=['W', 'UD'], default='W',
                        help='Main folder to perform the calculations.\nOptions are W --> /work/%s /// UD --> /UserData/%s' % (user, user))
    # Parse the arguments passed to the script
    args = parser.parse_args()

    # Main folder
    if args.mainDirID == 'W':  # work
        mainFolder = '/work/%s' % user
    else:  # UserData
        mainFolder = '/UserData/%s' % user

    # The command to be run to create the tags
    tagCmdString = 'MakeTagList'

    # run folder
    runFolder = os.path.join(mainFolder, 'r%d' % args.run)
    ok = checkAndCreateFolder(runFolder)
    if not ok:
        sys.exit(1)
    tagCmdString += ' -r %d' % args.run

    # Beamline
    if args.beamline not in [1, 2, 3]:
        print('ERROR: Requested beamline %d not available' % args.beamline)
        sys.exit(1)
    tagCmdString += ' -b %d' % args.beamline

    # tag folder and file
    tagFolder = os.path.join(mainFolder, 'tags')
    ok = checkAndCreateFolder(tagFolder)
    if not ok:
        sys.exit(1)
    tagFile = os.path.join(tagFolder, 'tags_%d.list' % args.run)
    tagCmdString += ' -out %s' % tagFile

    # Detectors: if none is specified, all are considered
    detectorString = ''
    if args.detectors:
        detectors = [x.strip() for x in args.detectors.split(',')]  # Get the detectors as a list of strings
        possibleDetectors = getDetectorsForRun(runID=args.run, bl=args.beamline)
        # Check if all the required detectors are present in the run
        for d in detectors:
            if d not in possibleDetectors:
                print('WARNING: %s not present in the run, ignoring it' % d)
            else:
                detectorString += '%s, ' % d
    if detectorString:  # There were some detectors specified and found in the possible ones
        tagCmdString += ' -det "%s"' % detectorString

    # conditions file
    if args.conditions:  # not an empty string
        if not os.path.isfile(args.conditions):  # check if file exist
            print('Warning: required conditions file not found, stopping...')
            sys.exit(1)
        tagCmdString += ' -inp %s' % args.conditions

    bkgFile = os.path.join(runFolder, 'bkg_%d.h5' % args.run)

    imgAvgStr = 'ImgAvg -l %s -out %s' % (tagFile, bkgFile)
    createRunJobFile(tagCmdString, imgAvgStr, runFolder, args.run)




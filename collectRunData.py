# Extract the average image for the detectors present in the configuration file and save it in the background folder

from argparse import ArgumentParser
from getpass import getuser  # to get the username
import os
import sys
import re


TWOCOLORSPECTROMETERCAMERA = ''
XTGCAMERA = ''
DIFFRACTIONCAMERA = ''


def getBunchIdInfo(run, bl=3):
    """
    Finds the shot numbers (start, end, number of shots) for a specific run
    Parameters
    ----------
    run: run number
    bl: (optional) the beamline

    Returns
    -------
    (start, end, number of shots)
    """
    stream = os.popen('ShowRunInfo -b %d -r %s' % (bl, run))
    # Read the stream
    out = stream.read()
    streamAsList = out.split('\n')
    '''
    Example
    streamAsList = ['Status   :Stopped (Ready to Read)', 'BeamLine :3', 'RunNumber:955255', 
                    'Start    :2020/11/19 19:33:11.770018', 'Stop     :2020/11/19 19:34:18.411914', 
                    'TriggerHi:202002', 'Trigger  :4000trigger 448440760-448444760', 'Comment  :bg', '']
    '''
    # Get the required element of the list
    info = [x for x in streamAsList if 'Trigger' in x and 'TriggerHi' not in x]
    if len(info) == 0:  # no info
        return None, None, None
    # Get the string, split using spaces
    info = info[0].split(' ')
    # e.g., info = ['Trigger', '', ':4000trigger', '448440760-448444760']
    # start/stop
    infoStartStop = info[-1].split('-')
    start = int(infoStartStop[0])
    stop = int(infoStartStop[1])
    # Number of shots
    nshots = int(re.findall(r'\d+', info[-2])[0])
    return start, stop, nshots


def createRunJobFile(cmdList, workingFolder, runID):
    """
    Creates the file to be submitted for the JOB and submits it.
    cmdList: a list containing all the commands that will need to be executed
    """
    cmdFile = os.path.join(workingFolder, 'JOB%d' % runID)
    with open(cmdFile, 'w') as f:
        f.write('#!/bin/bash\n')
        f.write('#\n')
        f.write('#PBS -l walltime=1:00:00\n')
        f.write('#PBS -l ncpus=1\n')
        f.write('#PBS -q serial\n')
        f.write('cd $PBS_O_WORKDIR\n')  # go to the same working directory as the one in which the script was launched
        for cmd in cmdList:
            f.write(cmd + '\n')
    os.system('chmod +x %s' % cmdFile)
    os.system('qsub %s' % cmdFile)


def getDetectorsForRun(runID, bl=3):
    """
    Get the detectors present in the data for the run and beamline
    Parameters
    ----------
    run: the run identifier
    bl: the beamline (default 3)

    Returns
    -------
    A list of detectors present
    """
    tmpFileName = 'out.out'
    res = os.system('ShowDetIDList -b %d -r %d > %s' % (bl, runID, tmpFileName))
    if not res == 0:  # failure
        print('WARNING: Cannot determine the detectors for run #%d (bl%d)' % (runID, bl))
        return []
    with open(tmpFileName) as f:
        mylist = f.read().splitlines()
    return mylist


def checkAndCreateFolder(folderName):
    """
    Checks and creates if necessary the folder needed.
    Returns success (bool)
    """
    if not os.path.isdir(folderName):
        try:
            os.mkdir(folderName)
        except Exception as e:
            print('ERROR: cannot create the run folder %s (%s)' % (folderName, e))
            return False
    return True


if __name__ == '__main__':
    """ Entrance method. """
    user = getuser()
    parser = ArgumentParser(description='Collect the data of one run, eventually specifying the background.')

    # Run number
    parser.add_argument('-r', type=int, dest='run', required=True, help='The run number to process')
    # Beamline
    parser.add_argument('-b', type=int, dest='beamline', default=3, help='The beamline (default 3)')
    # Detector(s)
    # Spectrometer for the two colors
    parser.add_argument('-SPEC', dest='spectrometer', action='store_true',
                        help='Whether to process the spectrometer image for the two colors.')
    parser.add_argument('-XTG', dest='xtg', action='store_true', help='Whether to process the XTG image.')
    parser.add_argument('-DIFF', dest='diffraction', action='store_true',
                        help='Whether to process the Diffraction image.')
    # Conditions
    parser.add_argument('-inp', type=str, dest='conditions', default='',
                        help='Full path to the Conditions file to be used. Default None')
    # Data directory
    parser.add_argument('-dir', type=str, dest='mainDirID', choices=['W', 'UD'], default='W',
                        help='Main folder to perform the calculations.\n' +
                             'Options are W --> /work/%s /// UD --> /UserData/%s' % (user, user))
    # (Eventually) how many shots
    parser.add_argument('-shots', type=int, dest='nshots', default=-1, help='How many shots to consider.')
    # Background file
    parser.add_argument('-bkg', type=str, dest='bkg', default='', help='Path to the background file to be used.')
    # Format file: contains specifications on how the output file will look like
    parser.add_argument('-format', type=str, dest='format', default='', help='Path to the format file to be used.')
    # Option for creating an image average file after creating the file
    parser.add_argument('-imgAvg', dest='imgAvg', action='store_true',
                        help='Whether to create an ImageAverage from the collected data.')
    # Option for forcing, i.e., deleting the folder if it exists
    parser.add_argument('-force', dest='force', action='store_true',
                        help='Whether to remove the folder, if exists, before collecting the data.')
    # Parse the arguments passed to the script
    args = parser.parse_args()

    # Main folder
    if args.mainDirID == 'W':  # work
        mainFolder = '/work/%s' % user
    else:  # UserData
        mainFolder = '/UserData/%s' % user

    # The command to be run to create the tags
    tagCmdString = 'MakeTagList'

    # run folder
    runFolder = os.path.join(mainFolder, 'r%d' % args.run)
    # Check if it exists and if force is selected
    if os.path.isdir(runFolder) and args.force:
        # Delete the run folder
        os.system('rm -rf %s' % runFolder)
    ok = checkAndCreateFolder(runFolder)
    if not ok:
        sys.exit(1)
    tagCmdString += ' -r %d' % args.run

    # Beamline
    if args.beamline not in [1, 2, 3]:
        print('ERROR: Requested beamline %d not available' % args.beamline)
        sys.exit(1)
    tagCmdString += ' -b %d' % args.beamline

    # tag folder and file
    tagFolder = os.path.join(mainFolder, 'tags')
    ok = checkAndCreateFolder(tagFolder)
    if not ok:
        sys.exit(1)
    tagFile = os.path.join(tagFolder, 'tags_%d.list' % args.run)
    tagCmdString += ' -out %s' % tagFile

    # Detectors: if none is specified, all are considered
    detectorString = ''
    possibleDetectors = getDetectorsForRun(runID=args.run, bl=args.beamline)

    if args.spectrometer:  # two-color spectrometer
        if TWOCOLORSPECTROMETERCAMERA in possibleDetectors:
            detectorString += TWOCOLORSPECTROMETERCAMERA + ','
        else:
            print('WARNING: two Color spectrometer camera (%s) not present in the run, ignoring it' %
                  TWOCOLORSPECTROMETERCAMERA)
    if args.xtg:  # transient grating
        if XTGCAMERA in possibleDetectors:
            detectorString += XTGCAMERA + ','
        else:
            print('WARNING: XTG camera (%s) not present in the run, ignoring it' % XTGCAMERA)
    if args.diffraction:
        if DIFFRACTIONCAMERA in possibleDetectors:
            detectorString += DIFFRACTIONCAMERA
        else:
            print('WARNING: Diffraction camera (%s) not present in the run, ignoring it' % DIFFRACTIONCAMERA)
    if detectorString:  # There were some detectors specified and found in the possible ones
        tagCmdString += ' -det "%s"' % detectorString

    # conditions file
    if args.conditions:  # not an empty string
        if not os.path.isfile(args.conditions):  # check if file exist
            print('Warning: required conditions file not found, stopping...')
            sys.exit(1)
        tagCmdString += ' -inp %s' % args.conditions

    # (Eventually) how many shots
    limitedNshots = False
    if args.nshots > 0:  # request only a certain amount of shots, to speed up the reduction and check the file
        startID, stopID, nshots = getBunchIdInfo(args.run, bl=args.beamline)
        if args.nshots < nshots:  # request of less shots than the ones present for the specific run
            tagCmdString += ' -starttag %d -endtag %d' % (startID, startID + args.nshots)
            limitedNshots = True

    # Format file: contains specifications on how the output file will look like
    # parser.add_argument('-format', type=str, dest='format', default='', help='Path to the format file to be used.')

    # Create the output file name
    if limitedNshots:  # add a suffix if only fewer shots were selected
        outputFile = 'run_%d_nshots_%d.h5' % (args.run, args.nshots)
    else:
        outputFile = 'run_%d.h5' % args.run

    # prepare the command for the data extraction
    dataExtractionCmd = 'DataConvert4 -l %s -dir %s -o %s' % (tagFile, runFolder, outputFile)
    # a background file was specified
    if args.bkg:
        # check if the file exists, prompt warning and exit otherwise
        if not os.path.isfile(args.bkg):
            print('Error: specified file for background subtraction not found, stopping...')
            sys.exit(1)
        dataExtractionCmd += ' -bkg %s' % args.bkg

    # if a format file was specified
    if args.format:
        # check if file exists, prompt warning and exit otherwise
        if not os.path.isfile(args.format):
            print('Error: specified format file not found, stopping...')
            sys.exit(1)
        dataExtractionCmd += ' -f %s' % args.format

    # create the list of commands to be executed
    cmdList = [tagCmdString, dataExtractionCmd]
    # if needed to generate an imageAverage file after the data retrieval
    if args.imgAvg:
        # Split the output file in name and extension
        outputFileParts = os.path.splitext(outputFile)
        if len(outputFileParts) > 1:  # check if the split makes sense
            # Combine the output file parts to generate the name of the average file
            imageFile = '%s_AVG%s' % (outputFileParts[0], outputFileParts[1])
            imgAvgCmd = 'ImgAvg -inp %s -out %s' % (outputFile, imageFile)
            # add command to command list
            cmdList.append(imgAvgCmd)
        else:
            print('Warning: cannot generate requested imageAverage file, skipping it...')
    createRunJobFile(cmdList, runFolder, args.run)



